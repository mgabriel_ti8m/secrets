package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestConvert(t *testing.T) {
	tests := []struct {
		description string
		reportPath  string
		wantReport  string
	}{
		{
			description: "no git",
			reportPath:  "testdata/reports/gitleaks-no-git.json",
			wantReport:  "testdata/expect/gl-secret-detection-report-no-git.json",
		},
		{
			description: "small full history",
			reportPath:  "testdata/reports/gitleaks-full-history-small.json",
			wantReport:  "testdata/expect/gl-secret-detection-report-full-history-small.json",
		},
		{
			description: "empty report",
			reportPath:  "testdata/reports/gitleaks-empty.json",
			wantReport:  "testdata/expect/gl-secret-detection-report-empty.json",
		},
	}

	for _, test := range tests {
		t.Log(test.description)
		f, err := os.Open(test.reportPath)
		if err != nil {
			t.Fatal(err)
		}
		got, err := convert(f, test.reportPath)
		if err != nil {
			t.Fatal(err)
		}
		f.Close()

		wantReportBytes, _ := ioutil.ReadFile(test.wantReport)
		gotReportBytes, err := json.MarshalIndent(got, "", "  ")
		if err != nil {
			t.Fatal(err)
		}

		assert.Equal(t, strings.Trim(string(wantReportBytes), "\n"), strings.Trim(string(gotReportBytes), "\n"))
	}
}
